<%-- 
    Document   : consultar
    Created on : 06-may-2021, 22:28:26
    Author     : Jonathan
--%>

<%@page import="ciisa.entity.Registropalabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% Registropalabras resultado = (Registropalabras)request.getAttribute("buscar");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Significado</title>
    </head>
    <body>
        
        <form  name="form" action="controladorconsulta" method="POST">
            <br>
            <br>
            <table class="table table-bordered">
                <tr><td class="table-primary"><b>Palabra Consultada</b></td><td ><%= resultado.getPalabras()%></td><td class="table-primary"><b>Definicion</b></td><td><%= resultado.getSignificado()%></td></tr>
                
            </table>
                <h2>Fuente: Oxford Dictionaries</h2>
                <center>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
                </center>
         </form>
    </body>
</html>
