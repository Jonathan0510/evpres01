<%-- 
    Document   : consultar
    Created on : 06-may-2021, 22:28:26
    Author     : Jonathan
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ciisa.entity.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Examen Jonathan Ahumada</title>
        <link rel="stylesheet" href="css/estilosform.css" type="text/css" media="all" />
	<script type="text/javascript" src="js/validarradio.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <%
    List<Diccionario> usuarios = (List<Diccionario>) request.getAttribute("Listadiccionario");
    Iterator<Diccionario> itPersona = usuarios.iterator();
    %>
    
<body>
 <form  name="form" onsubmit="return validacion(); action="controladorconsulta" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               <center>
               <table class="table table-bordered" >
                    <thead>
                    <th>ID</th>
                    <th>PALABRA</th>
                    <th>FECHA</th>             
                    <th> SIGNIFICADO</th>
                    </thead>
                    <tbody> 
                        <%while (itPersona.hasNext()) {
                       Diccionario per = itPersona.next();%>
                        <tr>
                            <td><%= per.getId()%></td>
                            <td><%= per.getPalabra()%></td>
                            <td><%= per.getFecha()%></td>
                          <td><%= per.getSignificado()%></td>                          
                        </tr>
                        <%}%>                
                    </body>           
                </table>
                    <br>
                    <br>
                   <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
              </center>

        
        </form>
</body>
</html>
