/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "registropalabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registropalabras.findAll", query = "SELECT r FROM Registropalabras r"),
    @NamedQuery(name = "Registropalabras.findByPalabras", query = "SELECT r FROM Registropalabras r WHERE r.palabras = :palabras"),
    @NamedQuery(name = "Registropalabras.findBySignificado", query = "SELECT r FROM Registropalabras r WHERE r.significado = :significado")})
public class Registropalabras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabras")
    private String palabras;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;

    public Registropalabras() {
    }

    public Registropalabras(String palabras) {
        this.palabras = palabras;
    }

    public String getPalabras() {
        return palabras;
    }

    public void setPalabras(String palabras) {
        this.palabras = palabras;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palabras != null ? palabras.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registropalabras)) {
            return false;
        }
        Registropalabras other = (Registropalabras) object;
        if ((this.palabras == null && other.palabras != null) || (this.palabras != null && !this.palabras.equals(other.palabras))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ciisa.entity.Registropalabras[ palabras=" + palabras + " ]";
    }
    
}
