/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.dao;

import ciisa.dao.exceptions.NonexistentEntityException;
import ciisa.dao.exceptions.PreexistingEntityException;
import ciisa.entity.Diccionario;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jonathan
 */
public class DiccionarioJpaController implements Serializable {
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    
    public DiccionarioJpaController() {
        
    }
    
    public DiccionarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    /*private EntityManagerFactory emf = null;*/

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Diccionario diccionario) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(diccionario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDiccionario(diccionario.getId()) != null) {
                throw new PreexistingEntityException("Diccionario " + diccionario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Diccionario diccionario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            diccionario = em.merge(diccionario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = diccionario.getId();
                if (findDiccionario(id) == null) {
                    throw new NonexistentEntityException("The diccionario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Diccionario diccionario;
            try {
                diccionario = em.getReference(Diccionario.class, id);
                diccionario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The diccionario with id " + id + " no longer exists.", enfe);
            }
            em.remove(diccionario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Diccionario> findDiccionarioEntities() {
        return findDiccionarioEntities(true, -1, -1);
    }

    public List<Diccionario> findDiccionarioEntities(int maxResults, int firstResult) {
        return findDiccionarioEntities(false, maxResults, firstResult);
    }

    private List<Diccionario> findDiccionarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Diccionario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Diccionario findDiccionario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Diccionario.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiccionarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Diccionario> rt = cq.from(Diccionario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
