/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.dao;

import ciisa.dao.exceptions.NonexistentEntityException;
import ciisa.dao.exceptions.PreexistingEntityException;
import ciisa.entity.Registropalabras;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jonathan
 */
public class RegistropalabrasJpaController implements Serializable {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    
     public RegistropalabrasJpaController() {
        this.emf = emf;
    }
    
    public RegistropalabrasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
   /* private EntityManagerFactory emf = null;*/

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registropalabras registropalabras) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registropalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistropalabras(registropalabras.getPalabras()) != null) {
                throw new PreexistingEntityException("Registropalabras " + registropalabras + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registropalabras registropalabras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registropalabras = em.merge(registropalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = registropalabras.getPalabras();
                if (findRegistropalabras(id) == null) {
                    throw new NonexistentEntityException("The registropalabras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registropalabras registropalabras;
            try {
                registropalabras = em.getReference(Registropalabras.class, id);
                registropalabras.getPalabras();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registropalabras with id " + id + " no longer exists.", enfe);
            }
            em.remove(registropalabras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registropalabras> findRegistropalabrasEntities() {
        return findRegistropalabrasEntities(true, -1, -1);
    }

    public List<Registropalabras> findRegistropalabrasEntities(int maxResults, int firstResult) {
        return findRegistropalabrasEntities(false, maxResults, firstResult);
    }

    private List<Registropalabras> findRegistropalabrasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registropalabras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registropalabras findRegistropalabras(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registropalabras.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistropalabrasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registropalabras> rt = cq.from(Registropalabras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
