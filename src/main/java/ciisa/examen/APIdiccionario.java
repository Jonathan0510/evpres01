/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.examen;

import ciisa.dao.DiccionarioJpaController;
import ciisa.dao.RegistropalabrasJpaController;
import ciisa.dto.PalabraDTO;
import ciisa.entity.Diccionario;
import ciisa.entity.Registropalabras;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jonathan
 */
public class APIdiccionario {
   
    
    @GET
    @Path("/{idbuscar}")
    @Produces (MediaType.APPLICATION_JSON)
    public Response significado (@PathParam("idbuscar") String idbuscar){
        Date fecha = new Date();
        DiccionarioJpaController dao = new DiccionarioJpaController();
        RegistropalabrasJpaController daopalbras =new RegistropalabrasJpaController();
       try { 
       Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+ idbuscar);
        
        PalabraDTO palabradto =  myResource.request(MediaType.APPLICATION_JSON).header("app_id","05db204c").header("app_key","fab01a2116f51780c195d9c8277dd8d6").get(PalabraDTO.class);
        
        String definicion = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
        
        
        Random numeroRandom = new Random();
        int upperbound = 1000;
        int intRandom = numeroRandom.nextInt(upperbound); 
        String idString = String.valueOf(intRandom);
        
        Diccionario diccionario = new Diccionario();
        Registropalabras registropalabras = new Registropalabras();
        
        diccionario.setId(idString);
        diccionario.setPalabra(palabradto.getWord());
        registropalabras.setPalabras(palabradto.getWord());
        diccionario.setFecha(fecha.toString());
        diccionario.setSignificado(definicion);
        registropalabras.setSignificado(definicion);
        
        
            dao.create(diccionario);
            daopalbras.create(registropalabras);
        } catch (Exception ex) {
            Logger.getLogger(APIdiccionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return null;
         
    }
    
}
