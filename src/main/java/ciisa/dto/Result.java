/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.dto;

import java.util.List;

/**
 *
 * @author Andrea
 */
public class Result{
    public String id;
    public String language;
    public List<LexicalEntry> lexicalEntries;
    public String type;
    public String word;
    
    
    public List<LexicalEntry> getLexicalEntries() {
        return this.lexicalEntries; 
    } 
    
    public void setLexicalEntries(List<LexicalEntry> lexicalEntries) {
        this.lexicalEntries = lexicalEntries; 
    } 
}
