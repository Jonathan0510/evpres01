/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import ciisa.dao.DiccionarioJpaController;
import ciisa.dao.RegistropalabrasJpaController;
import ciisa.entity.Diccionario;
import ciisa.entity.Registropalabras;
import ciisa.examen.APIdiccionario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jonathan
 */
@WebServlet(name = "controladorconsulta", urlPatterns = {"/controladorconsulta"})
public class controladorconsulta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controladorconsulta</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controladorconsulta at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton=request.getParameter("accion");
        DiccionarioJpaController dao = new DiccionarioJpaController();
        RegistropalabrasJpaController daopalbras =new RegistropalabrasJpaController();
        APIdiccionario api = new APIdiccionario();
        List Listadiccionario = dao.findDiccionarioEntities();
        Date fecha = new Date();
        
        
        
        if (boton.equals("consultar")) {

            String palabra = request.getParameter("palabra");
            
            api.significado(palabra);
           
            
            Registropalabras buscar = daopalbras.findRegistropalabras(palabra);
            if(daopalbras.findRegistropalabras(palabra)== null){
            request.getRequestDispatcher("retorno.jsp").forward(request, response);    
            }
            
            request.setAttribute("buscar", buscar);
            
            request.getRequestDispatcher("definicion.jsp").forward(request, response);
            
        }
        
        if (boton.equals("consultarDB")){
        
            Listadiccionario = dao.findDiccionarioEntities();
            request.setAttribute("Listadiccionario", Listadiccionario);  
            
            request.getRequestDispatcher("consulta2.jsp").forward(request, response);
       
       }
       if (boton.equals("volver")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
       } 
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
